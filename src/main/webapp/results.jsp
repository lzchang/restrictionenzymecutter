<%@ page import="nl.bioinf.lzchang.enzymecutter.model.Fragment" %><%--
  Created by IntelliJ IDEA.
  User: Lin
  Date: 5-12-2017
  Time: 23:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="includes/stylesheets.jsp" />
    <title>Restriction Enzyme Cutter - results</title>
</head>
<body>
<div class="container">
    <h1>Restriction Enzyme Cutter</h1>
    <h4>${requestScope.errorMsg}</h4>
    <h3>Results:</h3>
    <c:choose>
        <c:when test="${fragments != null}">
            <table class="table table-bordered">
                <tr>
                    <th>#</th>
                    <th>Start</th>
                    <th>Stop</th>
                    <th>Length</th>
                    <th>Molecular weight (g/mol)</th>
                    <th>GC%</th>
                    <th>Sequence</th>
                </tr>
                <c:forEach var="fragment" items="${fragments}" varStatus="index">
                    <tr>
                        <td>${index.count}</td>
                        <td>${fragment.getStart()}</td>
                        <td>${fragment.getStop()}</td>
                        <td>${fragment.getSequence().length()}</td>
                        <td>${fragment.getMolecularWeight()}</td>
                        <td>${fragment.getGcPercentage()}</td>
                        <td><a href="<c:url value="/download?fragment=${index.count}"/>">download</a></td>
                    </tr>

                </c:forEach>
            </table>
        </c:when>
        <c:otherwise>
            <c:redirect url="index.jsp" />
        </c:otherwise>
    </c:choose>

</div>
<jsp:include page="includes/footer.jsp" />
</body>
</html>
