<%--
  Created by IntelliJ IDEA.
  User: Lin
  Date: 5-12-2017
  Time: 22:48
  To change this template use File | Settings | File Templates.
--%>
    <footer class="footer">
        <div class="container">
            <div class="container text-center">
                <span class="text-muted">Copyright &copy; 2017 Lin Chang  ${initParam.admin_email}</span>
            </div>
        </div>
    </footer>