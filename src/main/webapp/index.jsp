<%--
  Created by IntelliJ IDEA.
  User: Lin
  Date: 4-12-2017
  Time: 23:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="includes/stylesheets.jsp"/>
    <link rel="stylesheet" type="text/css" href="css/sequenceForm.css"/>
    <title>Restriction Enzyme Cutter</title>
</head>
<body>

<nav class="navbar navbar-expand-sm bg-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="<c:url value="/index.jsp"/>">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="help" href="#help">Help</a>
        </li>
    </ul>
</nav>

<div class="container">
    <h1>Restriction Enzyme Cutter</h1>
    <div class="home">
        <form method="POST" name="sequenceForm" id="sequenceForm" action="<c:url value="/getfragments"/>"
              onsubmit="return validateSequence()">

            <label for="sequence">
                Enter DNA sequence here:<br/>
            </label>
            <textarea class="form-control" id="sequence" name="sequence" rows="8" required></textarea><br/>

            <label for="selectedEnzymes">
                Select restriction enzymes (select multiple by holding ctrl):<br/>
            </label>
            <select class="form-control" id="selectedEnzymes" name="selectedEnzymes" size="5" multiple required>
                <option value="AatII">AatII&emsp;(GACGTC)</option>
                <option value="Acc65I">Acc65I&emsp;(GGTACC)</option>
                <option value="AclI">AclI&emsp;(AACGTT)</option>
                <option value="AfeI">AfeI&emsp;(AGCGCT)</option>
                <option value="AgeI">AgeI&emsp;(ACCGGT)</option>
                <option value="BamHI">BamHI&emsp;(GGATCC)</option>
                <option value="BclI">BclI&emsp;(TGATCA)</option>
                <option value="BglII">BglII&emsp;(AGATCT)</option>
                <option value="BstBI">BstBI&emsp;(TTCGAA)</option>
                <option value="ClaI">ClaI&emsp;(ATCGAT)</option>
                <option value="DraI">DraI&emsp;(TTTAAA)</option>
                <option value="EcoRI">EcoRI&emsp;(GAATTC)</option>
                <option value="EcoRV">EcoRV&emsp;(GATATC)</option>
                <option value="HindIII">HindIII&emsp;(AAGCTT)</option>
                <option value="HpaI">HpaI&emsp;(GTTAAC)</option>
                <option value="MscI">MscI&emsp;(TGGCCA)</option>
                <option value="NotI">NotI&emsp;(GCGGCCGC)</option>
                <option value="PmlI">PmlI&emsp;(CACGTG)</option>
                <option value="PstI">PstI&emsp;(CTGCAG)</option>
            </select>
            <br/>
            <div class="error-msg alert alert-danger" role="alert"></div>
            <br/>
            <input type="submit" value="Analyze"/>
            <input type="reset" value="Reset"/>
        </form>
    </div>

    <div class="help">
        <p>
            The Restriction Enzyme Cutter is a web application which allows you to cut a given single DNA-sequence with
            the supported restriction enzymes.<br/>
            The restrictions enzymes are then applied to the DNA-sequence making cuts in the sequence.
            This results in cut fragments from the DNA-sequence with properties such as GC-content and the molecular
            weight
            of the fragment, with a button to download the sequence of the fragment.<br/>
            Currently 19 different restriction enzymes are supported.
        <p>

            The usage is as follows:
        <ol>
            <li>Enter a single DNA-sequence (FASTA-format accepted).</li>
            <li>Select one or more restriction enzymes to cut with.</li>
            <li>Press Analyze.</li>
        </ol>

        This should redirect you towards a page with the results, with the following columns present:

        <ul>
            <li># - Fragment number</li>
            <li>Start - The start position of the fragment within the sequence</li>
            <li>Stop - The end position of the fragment within the sequence</li>
            <li>Length - Length of the fragment in nucleotides</li>
            <li>Molecular weight - The molecular weight of the fragment in g/mol</li>
            <li>GC% - The GC-content</li>
            <li>Sequence - Button to download the fragment sequence in FASTA-format</li>
        </ul>

        Only DNA-sequence characters are allowed, which are A, C, T and G's, these are case insensitive.
    </div>
</div>
<jsp:include page="includes/footer.jsp"/>
</body>
<script src="js/libs/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/formValidator.js"></script>
</html>
