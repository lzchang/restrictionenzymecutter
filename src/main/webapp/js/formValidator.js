$(document).ready(function(){
    $("input[type='reset']").on('click', function(){
        $(".error-msg").fadeOut().empty();
    });

    $('a#help').on('click', function(){
        $('.home').hide();
        $('.help').show();
    });
});

function validateSequence() {
    var sequence = document.forms["sequenceForm"]["sequence"].value;
    sequence = sequence.trim();

    if (sequence.indexOf(">") === 0){
        if (!sequence.match("\n")){
            printErrorMsg("Error! Not a valid FASTA format");
            return false
        } else {
            sequence = sequence.substring(sequence.indexOf("\n") + 1);
        }
    }

    if (sequence.match(">")) {
        printErrorMsg("Error! Only one FASTA sequence allowed");
        return false
    }
    else if (sequence.match("[0-9]?[^AGTCagtc]")){
        printErrorMsg("Error! Only DNA sequences characters allowed (ACTG)");
        return false
    }
}

function printErrorMsg(text){
    $(".error-msg").empty().text(text).fadeIn().delay(5000).fadeOut();
}


