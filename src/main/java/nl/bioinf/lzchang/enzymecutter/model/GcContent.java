/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.enzymecutter.model;

/**
 * Calculates the GC-percentage of a sequence/fragment.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class GcContent {
    /**
     * Reads the sequence and calculates the GC-percentage.
     * @param sequence is the DNA sequence
     * @return gcPercentage
     */
    public int calculateGcPercentage(String sequence) {
        int gcAmount = 0;
        for (Character nucleotide : sequence.toUpperCase().toCharArray()) {
            if (nucleotide.equals('G') || nucleotide.equals('C')) {
                gcAmount++;
            }
        }
        return (int) ((double) gcAmount / sequence.length() * 100);
    }
}
