/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.enzymecutter.model;

/**
 * Class for storing DNA-sequences.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class Sequence {
    private String sequence;

    public Sequence(String sequence) {
        this.sequence = sequence;
    }

    /**
     * Get the DNA sequence.
     * @return sequence
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * Get the molecular weight of the sequence.
     * @return molecularWeight
     */
    public String getMolecularWeight() {
        MolecularWeight weight = new MolecularWeight();
        return weight.calculateWeight(sequence);
    }

    /**
     * Get the GC-percentage of the sequence.
     * @return GcPercentage
     */
    public int getGcPercentage() {
        GcContent GcPercentage = new GcContent();
        return GcPercentage.calculateGcPercentage(sequence);
    }

    @Override
    public String toString() {
        return sequence.replaceAll("(.{70})", "$1\n");
    }
}
