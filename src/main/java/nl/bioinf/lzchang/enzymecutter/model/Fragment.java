/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.enzymecutter.model;

/**
 * Fragments of DNA-sequence with the start and stop locations of the fragments
 * in the sequence.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class Fragment extends Sequence {
    private final int startInSequence;
    private final int stopInSequence;

    public Fragment(String sequence, int start, int stop) {
        super(sequence);
        this.startInSequence = start;
        this.stopInSequence = stop;
    }

    /**
     * Returns the start position of fragment in full sequence.
     * @return startInSequence
     */
    public int getStart() {
        return startInSequence;
    }

    /**
     * Returns the stop position of fragment in full sequence.
     * @return stopInSequence
     */
    public int getStop() {
        return stopInSequence;
    }
}
