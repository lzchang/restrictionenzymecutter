/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.enzymecutter.model;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Calculates the molecular weight of sequences/fragments.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class MolecularWeight {
    private static final Map<Character, Double> nucleotideWeights;

    static {
        nucleotideWeights = new HashMap<>();
        nucleotideWeights.put('A', 135.13);
        nucleotideWeights.put('G', 151.13);
        nucleotideWeights.put('T', 126.12);
        nucleotideWeights.put('C', 111.10);
    }

    /**
     * Calculates the total molecular weight
     * @param sequence DNA sequence
     * @return totalWeight
     */
    public String calculateWeight(String sequence) {
        Double totalWeight = 0.0;
        for (Character nucleotide : sequence.toCharArray()) {
            totalWeight += nucleotideWeights.get(nucleotide);
        }
        return String.format(Locale.US, "%.1f", totalWeight);
    }
}
