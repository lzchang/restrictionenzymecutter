/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.enzymecutter.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

/**
 * This class parses the DNA-sequence given as input and returns a DNA-sequence
 * without non-word characters.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class SequenceParser {
    private String sequence;

    public SequenceParser(String sequence) {
        this.sequence = sequence;
    }

    /**
     * Reads the given DNA sequence in the web form, Changes all nucleotide characters to uppercase.
     * @return sequence
     * @throws IOException
     */
    public String parseString() throws IOException {
        BufferedReader reader = new BufferedReader(new StringReader(sequence));
        StringBuilder sequence = new StringBuilder();
        String line;

        while ((line = reader.readLine()) != null) {
            if (!line.startsWith(">")) {
                sequence.append(line);
            }
        }
        return sequence.toString().replaceAll("\\W", "").toUpperCase();
    }
}
