/*
 * Copyright (c) 2017. Lin Chang [l.z.chang@st.hanze.nl].
 * All rights reserved.
 */

package nl.bioinf.lzchang.enzymecutter.io;

import nl.bioinf.lzchang.enzymecutter.model.Fragment;
import nl.bioinf.lzchang.enzymecutter.model.Sequence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Creates fragments of a DNA-sequence using a list of selected enzymes.
 * @Author Lin Chang [l.z.chang@st.hanze.nl]
 * @version 0.0.1
 */
public class MakeFragments {
    private Sequence sequence;
    private String[] selectedEnzymes;
    private ArrayList<Fragment> madeFragments = new ArrayList<>();
    private static final Map<String, ArrayList<String>> restrictionEnzymes;

    static {
        restrictionEnzymes = new HashMap<>();
        restrictionEnzymes.put("AatII", new ArrayList<>(Arrays.asList("GACGTC", "GACGT|_|C")));
        restrictionEnzymes.put("Acc65I", new ArrayList<>(Arrays.asList("GGTACC", "G|_|GTACC")));
        restrictionEnzymes.put("AclI", new ArrayList<>(Arrays.asList("AACGTT", "AA|_|CGTT")));
        restrictionEnzymes.put("AfeI", new ArrayList<>(Arrays.asList("AGCGCT", "AGC|_|GCT")));
        restrictionEnzymes.put("AgeI", new ArrayList<>(Arrays.asList("ACCGGT", "A|_|CCGGT")));
        restrictionEnzymes.put("BamHI", new ArrayList<>(Arrays.asList("GGATCC", "G|_|GATCC")));
        restrictionEnzymes.put("BclI", new ArrayList<>(Arrays.asList("TGATCA", "T|_|GATCA")));
        restrictionEnzymes.put("BglII", new ArrayList<>(Arrays.asList("AGATCT", "A|_|GATCT")));
        restrictionEnzymes.put("BstBI", new ArrayList<>(Arrays.asList("TTCGAA", "TT|_|CGAA")));
        restrictionEnzymes.put("ClaI", new ArrayList<>(Arrays.asList("ATCGAT", "AT|_|CGAT")));
        restrictionEnzymes.put("DraI", new ArrayList<>(Arrays.asList("TTTAAA", "TTT|_|AAA")));
        restrictionEnzymes.put("EcoRI", new ArrayList<>(Arrays.asList("GAATTC", "G|_|AATTC")));
        restrictionEnzymes.put("EcoRV", new ArrayList<>(Arrays.asList("GATATC", "GAT|_|ATC")));
        restrictionEnzymes.put("HindIII", new ArrayList<>(Arrays.asList("AAGCTT", "A|_|AGCTT")));
        restrictionEnzymes.put("HpaI", new ArrayList<>(Arrays.asList("GTTAAC", "GTT|_|AAC")));
        restrictionEnzymes.put("MscI", new ArrayList<>(Arrays.asList("TGGCCA", "TGG|_|CCA")));
        restrictionEnzymes.put("NotI", new ArrayList<>(Arrays.asList("GCGGCCGC", "GC|_|GGCCGC")));
        restrictionEnzymes.put("PmlI", new ArrayList<>(Arrays.asList("CACGTG", "CAC|_|GTG")));
        restrictionEnzymes.put("PstI", new ArrayList<>(Arrays.asList("CTGCAG", "CTGCA|_|G")));
    }

    public MakeFragments(Sequence sequence, String[] selectedEnzymes) {
        this.sequence = sequence;
        this.selectedEnzymes = selectedEnzymes;
    }

    /**
     * Apply the selected enzymes and put markers (|) around the fragments and a marker (_)
     * for splitting the fragments with.
     */
    public void applyEnzymes() {
        String totalSequence = sequence.getSequence();
        for (String enzyme : selectedEnzymes) {
            totalSequence = totalSequence.replaceAll(restrictionEnzymes.get(enzyme).get(0),
                    restrictionEnzymes.get(enzyme).get(1));
        }

        createFragments("|" + totalSequence + "|");
    }

    /**
     * Split the Sequence resulting in fragments, save every fragment with amount of occurrences
     * in a HashMap, in case of duplicate fragments resulting in wrong start/stop positions.
     * @param sequenceWithMarkers DNA sequence
     */
    private void createFragments(String sequenceWithMarkers) {
        HashMap<String, Integer> fragmentHashMap = new HashMap<>();
        int amountOfMarkers = 1;

        for (String vb : sequenceWithMarkers.split("_")) {
            if (fragmentHashMap.containsKey(vb)) {
                madeFragments.add(new Fragment(vb.substring(1, vb.length() - 1), // Ignore markers indicating start/end
                        sequenceWithMarkers.indexOf(vb, sequenceWithMarkers.indexOf(vb) + fragmentHashMap.get(vb)) - amountOfMarkers + 2,
                        sequenceWithMarkers.indexOf(vb, sequenceWithMarkers.indexOf(vb) + fragmentHashMap.get(vb)) + vb.length() - amountOfMarkers - 1));
                fragmentHashMap.put(vb, fragmentHashMap.get(vb) + 1);

            } else {
                madeFragments.add(new Fragment(vb.substring(1, vb.length() - 1),
                        sequenceWithMarkers.indexOf(vb) - amountOfMarkers + 2,
                        sequenceWithMarkers.indexOf(vb) + vb.length() - amountOfMarkers - 1));
                fragmentHashMap.put(vb, 1);
            }
            amountOfMarkers += 3; // Marker characters (|_|) to ignore
        }
    }

    /**
     * Get a ArrayList of all the made fragments.
     * @return madeFragments
     */
    public ArrayList<Fragment> getFragments() {
        return madeFragments;
    }
}

