package nl.bioinf.lzchang.enzymecutter.servlets;

import nl.bioinf.lzchang.enzymecutter.model.Fragment;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;

@WebServlet(name = "downloadFile", urlPatterns = "/download")
public class downloadFile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        ArrayList<Fragment> totalFragments = (ArrayList<Fragment>) request.getSession().getAttribute("fragments");

        int fragmentIndex = Integer.parseInt(request.getParameter("fragment").replaceAll("[^\\d](\\d*)?", ""));
        if (session.isNew() ||
                session.getAttribute("fragments") == null) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
            dispatcher.forward(request, response);
        }
        // If user changes download URL manually
        else if (request.getParameter("fragment") == null ||
                fragmentIndex - 1 >= totalFragments.size()) {
            request.setAttribute("errorMsg", "The requested download file does not exist!");
            RequestDispatcher dispatcher = request.getRequestDispatcher("results.jsp");
            dispatcher.forward(request, response);
        } else {
            Fragment sequenceToWrite = totalFragments.get(fragmentIndex - 1);

            String file = "fragment.fa";

            prepareFile(file, sequenceToWrite);
            prepareDownload(file, response);
        }
    }

    /**
     * Prepares the file for download by making it available in the outputStream
     * @param file filename
     * @param response servletResponse
     * @throws IOException
     */
    private void prepareDownload(String file, HttpServletResponse response) throws IOException {
        File downloadFile = new File(file);
        FileInputStream inputStream = new FileInputStream(downloadFile);

        // forces download
        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
        response.setHeader(headerKey, headerValue);

        OutputStream outputStream = response.getOutputStream();

        byte[] buffer = new byte[4096];
        int bytesRead;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        // Cleanup
        inputStream.close();
        outputStream.close();
        downloadFile.delete();
    }

    /**
     * Writes DNA sequence to new file
     * @param file filename
     * @param sequenceToWrite the
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    private void prepareFile(String file, Fragment sequenceToWrite) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter(file, "UTF-8");
        writer.println(sequenceToWrite);
        writer.close();
    }
}
