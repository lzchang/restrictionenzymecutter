package nl.bioinf.lzchang.enzymecutter.servlets;

import nl.bioinf.lzchang.enzymecutter.io.SequenceParser;
import nl.bioinf.lzchang.enzymecutter.model.Fragment;
import nl.bioinf.lzchang.enzymecutter.io.MakeFragments;
import nl.bioinf.lzchang.enzymecutter.model.Sequence;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "getFragments", urlPatterns = "/getfragments")
public class getFragments extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] selectedEnzymes = request.getParameterValues("selectedEnzymes");
        String seq = request.getParameter("sequence");

        // Parse the sequence and create fragments from it
        Sequence sequence = parseString(seq);
        ArrayList<Fragment> madeFragments = createFragments(sequence, selectedEnzymes);

        HttpSession session = request.getSession();
        session.setAttribute("fragments", madeFragments);

        RequestDispatcher dispatcher = request.getRequestDispatcher("results.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Create fragments by cutting the sequence with the selected enzymes
     * @param sequence entered sequence
     * @param selectedEnzymes selected enzymes
     * @return madeFragments
     */
    private ArrayList<Fragment> createFragments(Sequence sequence, String[] selectedEnzymes) {
        MakeFragments fragments = new MakeFragments(sequence, selectedEnzymes);
        fragments.applyEnzymes();
        return fragments.getFragments();
    }

    /**
     * Parses the entered sequence
     * @param seq sequence
     * @return Sequence
     * @throws IOException
     */
    private Sequence parseString(String seq) throws IOException {
        SequenceParser parser = new SequenceParser(seq);
        return new Sequence(parser.parseString());
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("index.jsp");
        dispatcher.forward(request, response);
    }
}
