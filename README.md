#Restriction Enzyme Cutter

Cuts a given single DNA-sequence with the supported restriction enzymes.

Resulting in cut fragments from the DNA-sequence with properties such as GC-content and the molecular weight of the fragment, with a button to download the sequence of the fragment. 

##Installation

* Download from source (git clone, zipped package)
* Run index.jsp, located in RestrictionEnzymeCutter/src/main/webapp/ within Apache Tomcat

##Usage

1. Enter a single DNA-sequence (FASTA-format accepted).
2. Select one or more restriction enzymes to cut with.
3. Press Analyze.

This should redirect you towards a page with the results, with the following columns present:

* \# - Fragment number
* Start - The start position of the fragment within the sequence
* Stop - The end position of the fragment within the sequence
* Length - Length of the fragment in nucleotides
* Molecular weight - The molecular weight of the fragment in g/mol
* GC% - The GC-content 
* Sequence - Button to download the fragment sequence in FASTA-format

##Required software

* Apache Tomcat or comparable web container